from django.apps import AppConfig


class TransfomersApiConfig(AppConfig):
    name = 'transformers_api'
